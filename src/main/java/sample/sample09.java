package sample;

import java.util.LinkedHashMap;
import java.util.Map;

public class sample09 {
    public static void main(String[] args) {
//        String str = "ServiceNow";
        String str = "abc";
        str = str.toLowerCase();

        Map<Character, Integer> map = new LinkedHashMap<>();

        for (Character charValue : str.toCharArray()) {
            map.merge(charValue, 1, Integer::sum);
        }

        int index = 0;
        boolean isFound = false;
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            if (entry.getValue() == 1) {
                isFound = true;
                System.out.println(index);
                break;
            }
            index++;
        }

        if (!isFound) {
            System.out.println(-1);
        }

    }
}
