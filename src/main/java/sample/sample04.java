package sample;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class sample04 {
    public static void main(String[] args) {

        String inp = "!123!@#$Abcd^%";

        Pattern digit = Pattern.compile("//b//d");
        Matcher matchDig = digit.matcher(inp);

        Pattern word = Pattern.compile("//b//w");
        Matcher matchWord = word.matcher(inp);

        Pattern spChar = Pattern.compile("//b[!@#$^%]");
        Matcher matchSpChar = spChar.matcher(inp);

        HashMap<String, List<String>> map = new HashMap<>();
        ArrayList<String> list1 = new ArrayList<>();
        ArrayList<String> list2 = new ArrayList<>();
        ArrayList<String> list3 = new ArrayList<>();

        while(matchDig.find() || matchWord.find() || matchSpChar.find()){
            list1.add(matchDig.group());
            list2.add(matchWord.group());
            list3.add(matchSpChar.group());

            map.put("digit",list1);
            map.put("word",list2);
            map.put("spChar",list3);
        }

        System.out.println(list3.toString() + list1.toString() + list2.toString());

    }
}
