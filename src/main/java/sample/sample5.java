package sample;

class Base {

    public static void display(){
        System.out.println("base display");
    }

    public void print(){
        System.out.println("base print");
    }
}

class Derived extends Base {

    public static void display(){
        System.out.println("Derived display");
    }

    public void print(){
        System.out.println("Derived print");
    }
}

public class sample5 {
    public static void main(String[] args) {
        Base b1 = new Base();
        b1.display();
        b1.print();

        Base b2 = new Derived();
        b2.display();
        b2.print();

        Derived d1= new Derived();
        d1.print();

    }
}
