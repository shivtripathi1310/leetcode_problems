package interview.basic.array;

import java.util.Arrays;

public class TripletForGivenArraySum {
    public static void main(String[] args) {
        int[] arr = {1, 4, 2, 3, 5, 7, 8, 6, 9};
        int sum = 15;

        generateTriplet(arr,sum);
    }

    private static void generateTriplet(int[] arr, int sum) {

        Arrays.sort(arr);
        int length = arr.length;

        int l,r;
        for (int i=0;i<length-2;i++){
            l = i+1;
            r=length-1;
            while (l<r){
                if(arr[i] + arr[l] + arr[r] == sum) {
                    System.out.printf("%d : %d : %d%n", arr[i], arr[l], arr[r]);
                    l++;
                    r--;
                }
                else if(arr[i] + arr[l] + arr[r] < sum)
                    l++;
                else
                   r--;
            }
        }
    }
}
