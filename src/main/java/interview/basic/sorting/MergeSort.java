package interview.basic.sorting;

import java.util.Arrays;

//Divide + sort and merge sorted array(comparing left and right array)
public class MergeSort {
    public static void main(String[] args) {
        int[] arr = {5, 2, 1, 4, 7, 6};
        int l = 0;
        int r = arr.length - 1;

        sort(arr, l, r);
        System.out.println(Arrays.toString(arr));
    }

    // TC: O(nlogn)
    private static void sort(int[] arr, int l, int r) {

        if (l >= r)
            return;

        int mid = (l + r) / 2 ;
        sort(arr, l, mid);
        sort(arr, mid + 1, r);

        merge(arr, l, mid, r);
    }

    private static void merge(int[] arr, int l, int mid, int r) {

        int n1 = mid - l + 1;
        int n2 = r - mid;

        int[] left = new int[n1];
        int[] right = new int[n2];

        //copying all left array
        for (int i = 0; i < n1; i++)
            left[i] = arr[l + i];

        //copying all right array
        for (int i = 0; i < n2; i++)
            right[i] = arr[mid + i + 1];

        int i = 0;
        int j = 0;
        int k = l;  // Imp -> array initialization will start from l

        while (i < n1 && j < n2) {
            if (left[i] <= right[j]) {
                arr[k] = left[i];
                i++;
            } else if (left[i] > right[j]) {
                arr[k] = right[j];
                j++;
            }
            k++;
        }

        while (i < n1)
            arr[k++] = left[i++];
        while (j < n2)
            arr[k++] = right[j++];
    }
}
