package interview.basic.string;

/**
 * Oracle Interview Question
 */
public class PatternInCharacter {
    public static void main(String[] args) {
        String input = "!123!@#$Abcd^%";
        
        String output = rearrangeString(input);

        System.out.println(output);
    }

    public static String rearrangeString(String input) {
        StringBuilder digits = new StringBuilder();
        StringBuilder specialChars = new StringBuilder();
        StringBuilder alphabets = new StringBuilder();

        for (char c : input.toCharArray()) {
            if (Character.isDigit(c)) {
                digits.append(c);
            } else if (Character.isLetter(c)) {
                alphabets.append(c);
            } else {
                specialChars.append(c);
            }
        }

        StringBuilder result = new StringBuilder();
        result.append(specialChars);
        result.append(digits);
        result.append(alphabets);

        return result.toString();
    }
}
