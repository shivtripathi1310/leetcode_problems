package org.contest.hackerearth.stringmanipulation;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class problem01 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int inp = s.nextInt();

        List<String> list = new ArrayList<>();
        for(int i=0;i<inp;i++){
            list.add(s.next());
        }

        StringBuilder str;
        int length = 0;
        char middlechar = 0;
        for(String value : list){
            str = new StringBuilder(value);

            if(list.contains(String.valueOf(str.reverse()))){
                length =  value.length();
                middlechar = value.charAt(value.length()/2);
            }
        }

        System.out.print(length + " " + middlechar);
    }
}
