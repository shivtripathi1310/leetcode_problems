package org.contest.leetcode;

/**
 * https://leetcode.com/problems/is-subsequence/
 */
public class IsSubsequence {
    public static void main(String[] args) {
        String s = "abc";
        String t = "abwerc";
        System.out.println(isSubsequence(s, t));
    }

    public static boolean isSubsequence(String s, String t) {
        if(s==null || s.length()==0 )
            return true;

        int[][] dp = new int[s.length() + 1][t.length() + 1];

        int n1 = s.length();
        int n2 = t.length();

        char[] sArr = s.toCharArray();
        char[] tArr = t.toCharArray();

        for (int i = 0; i <= n1; i++) {
            for (int j = 0; j <= n2; j++) {
                if (i == 0 || j == 0) {
                    dp[i][j] = 0;
                }
            }
        }

        for (int i = 1; i <= n1; i++) {
            for (int j = 1; j <= n2; j++) {
                if (sArr[i - 1] == tArr[j - 1]) {
                    dp[i][j] = 1+ dp[i - 1][j - 1];
                } else {
                    dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
                }
            }
        }
        System.out.println(dp[n1][n2]);

        if (dp[n1][n2] == n1) {
            return true;
        } else {
            return false;
        }
    }
}
