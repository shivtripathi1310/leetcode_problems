package org.contest.leetcode;

/**
 * https://leetcode.com/problems/find-pivot-index/
 */
public class FindPivotIndex {
    public int pivotIndex(int[] nums) {
        int sum = 0, leftsum = 0;

        //Here we are calculating sum of all array elements
        for (int x: nums)
            sum += x;

        //0(n) -> Iterating in an array
        for (int i = 0; i < nums.length; ++i) {

            // Here, checking leftsum is equal to (right sum = sum - leftsum - nums[i])
            if (leftsum == sum - leftsum - nums[i])
                return i;

            //if leftsum != rightsum then, add th enums[i] to leftsum
            leftsum += nums[i];
        }

        //if after iterating whole array leftsum  != rightsum then return -1
        return -1;
    }
}
