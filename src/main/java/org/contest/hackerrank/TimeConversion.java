package org.contest.hackerrank;

/**
 * https://www.hackerrank.com/challenges/one-month-preparation-kit-time-conversion/problem
 */
public class TimeConversion {

    public static String timeConversion(String s) {
        // Write your code here
        int hour = Integer.parseInt(s.substring(0,s.indexOf(":")));
        String time = s.substring(s.length()-2);

        if(time.equals("PM")){
            if(hour !=12){
                hour = 12 + hour;
            }
        }else if(time.equals("AM")){
            if(hour ==12){
                hour = 0;
            }
        }
        String hh = String.valueOf(hour);
        if(hh.length()==1){
            hh = "0" + hh;
        }
        String converedTime =  (hh + s.substring(s.indexOf(":")));
        return converedTime.substring(0,s.length()-2);
    }
}
