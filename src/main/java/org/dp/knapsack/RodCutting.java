package org.dp.knapsack;

/**
 * A Rod of length k has to be made, a length array and price array is given.
 * Same rod can be used more than 1 time.
 * Find the max profit.
 */
public class RodCutting {
    public static void main(String[] args) {
        int[] length = {1, 2, 3, 4};
        int[] price = {5, 6, 2, 9};
        int totalLength = 4;

        int[][] dp = new int[length.length + 1][totalLength + 1];
        System.out.println(maxProfitOfRodCutting(length, price, dp, totalLength));
    }

    private static int maxProfitOfRodCutting(int[] length, int[] price, int[][] dp, int totalLength) {
        int n = length.length;
        for (int i = 0; i <= n; i++) {
            for (int j = 0; j <= totalLength; j++) {
                if (i == 0 || j == 0) {
                    dp[i][j] = 0;
                }
            }
        }

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= totalLength; j++) {
                if(length[i-1]<=j){
                    dp[i][j] = Math.max(price[i-1] + dp[i][j-length[i-1]],dp[i-1][j]);
                }else{
                    dp[i][j] = dp[i-1][j];
                }
            }
        }

        return dp[n][totalLength];
    }
}
