package org.dp.knapsack;

import java.util.Arrays;

public class Knapsack01Memoization {
    static int[][] dp= new int[50][50];
    public static void main(String[] args) {
        Integer[] wt = {1, 2, 3, 4, 5};
        Integer[] val = {4, 2, 4, 5, 3};
        int totalWeight = 7;
        for(int[] arr : dp){
            Arrays.fill(arr,-1);
        }
        System.out.println("Knapsack : Max profit : " + solveKnapsack01(totalWeight, wt, val, wt.length));
    }

    private static int solveKnapsack01(int W, Integer[] wt, Integer[] vt, int n) {
        if(n==0 || W==0){
            return 0;
        }

        if(dp[n][W]!=-1){
            return dp[n][W];
        }

        if(wt[n-1]<=W){
           return dp[n][W] = Math.max(vt[n-1] + solveKnapsack01(W-wt[n-1],wt,vt,n-1),solveKnapsack01(W,wt,vt,n-1));
        }else {
            return dp[n][W] = solveKnapsack01(W,wt,vt,n-1);
        }
    }
}
