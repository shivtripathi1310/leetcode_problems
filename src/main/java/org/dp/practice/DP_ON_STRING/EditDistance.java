package org.dp.practice.DP_ON_STRING;

import java.util.Arrays;

/**
 * Here , we are given 2 strings, change string 01 to string 02
 * Insert , delete, Replace operations have to be done to string to 02.
 * Find the min. operations needed to change the string
 */
public class EditDistance {
    public static void main(String[] args) {
        String s = "horse";
        String t = "rdos";
        int m = s.length();
        int n = t.length();

        int[][] dp = new int[m + 1][n + 1];
        for (int i = 0; i <= m; i++) {
            Arrays.fill(dp[i], -1);
        }

        System.out.println(minDistRecursion(s, t, m - 1, n - 1));
        System.out.println(minDistMemoization(s, t, m - 1, n - 1, dp));
        System.out.println(minDistBottomUp(s, t));
        System.out.println(minDistBottomUpSpaceOptimisation(s, t));
    }

    //Recursion TC: O(3^n) , SC: O(m+n) -> stack space
    private static int minDistRecursion(String s1, String s2, int m, int n) {
        // BC when s1 is empty, then n+1 insertion
        if (m < 0) return n + 1;

        // BC when s2 is empty, then m+1 deletion
        if (n < 0) return m + 1;

        if (s1.charAt(m) == s2.charAt(n)) {
            return minDistRecursion(s1, s2, m - 1, n - 1);
        } else {
            return Math.min(
                    1 + minDistRecursion(s1, s2, m, n - 1), //Insert
                    Math.min(1 + minDistRecursion(s1, s2, m - 1, n), //Delete
                            1 + minDistRecursion(s1, s2, m - 1, n - 1))); //Replace
        }
    }

    //Memoization TC: O(m*n) , SC: O(m*n) + O(m+n) -> stack space
    private static int minDistMemoization(String s1, String s2, int m, int n, int[][] dp) {
        // BC when s1 is empty, then n+1 insertion
        if (m < 0) return n + 1;

        // BC when s2 is empty, then m+1 deletion
        if (n < 0) return m + 1;

        if (dp[m][n] != -1) return dp[m][n];

        if (s1.charAt(m) == s2.charAt(n)) {
            return dp[m][n] = minDistMemoization(s1, s2, m - 1, n - 1, dp);
        } else {
            return dp[m][n] = Math.min(
                    1 + minDistMemoization(s1, s2, m, n - 1, dp), //Insert
                    Math.min(1 + minDistMemoization(s1, s2, m - 1, n, dp), //Delete
                            1 + minDistMemoization(s1, s2, m - 1, n - 1, dp))); //Replace
        }
    }

    //BottomUp TC: O(m*n) , SC: O(m*n)
    private static int minDistBottomUp(String s1, String s2) {
        int m = s1.length();
        int n = s2.length();
        int[][] dp = new int[m + 1][n + 1];

        for (int i = 0; i <= m; i++) {
            for (int j = 0; j <= n; j++) {
                if (i == 0 && j == 0)
                    dp[i][j] = 0;
                else if (i == 0) {
                    dp[i][j] = j;   //insertion
                } else if (j == 0) {
                    dp[i][j] = i;  //deletion
                }
            }
        }

        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {

                if (s1.charAt(i - 1) == s2.charAt(j - 1)) {
                    dp[i][j] = dp[i - 1][j - 1];
                } else {
                    dp[i][j] = Math.min(
                            1 + dp[i][j - 1], //Insert
                            Math.min(1 + dp[i - 1][j], //Delete
                                    1 + dp[i - 1][j - 1])); //Replace
                }
            }
        }

        return dp[m][n];
    }

    //BottomUp + SpaceOptimisation TC: O(m*n) , SC: O(n)..
    private static int minDistBottomUpSpaceOptimisation(String s1, String s2) {
        int m = s1.length();
        int n = s2.length();
        int[] prevRow = new int[n + 1];

        for (int j = 0; j <= n; j++) {
            prevRow[j] = j;  //Insertion
        }

        for (int i = 1; i <= m; i++) {
            int[] temp = new int[n + 1];
            temp[0] = i;
            for (int j = 1; j <= n; j++) {

                if (s1.charAt(i - 1) == s2.charAt(j - 1)) {
                    temp[j] = prevRow[j - 1];
                } else {
                    temp[j] = Math.min(
                            1 + temp[j - 1], //Insert
                            Math.min(1 + prevRow[j], //Delete
                                    1 + prevRow[j - 1])); //Replace
                }
            }
            prevRow = temp;
        }

        return prevRow[n];
    }
}
