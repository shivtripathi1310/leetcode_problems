package org.dp.practice.DP_ON_STRING;

/**
 * Two string are given , we have to find Shortest Common Supersequence
 * x- abcd
 * y- bde
 * LCS -> bd - 2
 * Merge x, y in such a way that it is supersequence of both x, y and it is shortest.
 * Shortest Common Supersequence -> x.length() + y.length() - lcs
 */
public class ShortestCommonSupersequence {

    public static void main(String[] args) {
        String x = "abcdeabcd";
        String y = "qweaebce";

        int[][] dp = new int[x.length() + 1][y.length() + 1];

        int lcs = findLCS(dp, x, y);

        int scs = x.length() + y.length() - lcs;
        System.out.println("ShortestCommonSupersequence : " + scs);
        System.out.println("ShortestCommonSupersequence String : " + printSCSString(dp,x,y));

    }

    private static int findLCS(int[][] dp, String x, String y) {

        int xLength = x.length();
        int yLength = y.length();
        char[] xArr = x.toCharArray();
        char[] yArr = y.toCharArray();

        for (int i = 0; i <= xLength; i++) {
            for (int j = 0; j <= yLength; j++) {
                if (i == 0 || j == 0) {
                    dp[i][j] = 0;
                }
            }
        }

        for (int i = 1; i <= xLength; i++) {
            for (int j = 1; j <= yLength; j++) {
                if(xArr[i-1]==yArr[j-1]){
                    dp[i][j]= 1 + dp[i-1][j-1];
                }else {
                    dp[i][j]= Math.max(dp[i-1][j],dp[i][j-1]);
                }
            }
        }

        // Finding the max value in 2D Array
        int lcs = 0;
        for(int i=0;i<xLength+1;i++){
            for(int j=0;j<yLength+1;j++){
                lcs = Math.max(lcs,dp[i][j]);
            }
        }
        return lcs;

    }

    private static String printSCSString(int[][]dp, String a, String b){
        char[] aArr = a.toCharArray();
        char[] bArr = b.toCharArray();

        int n1 = a.length();
        int n2 = b.length();

        StringBuilder builder = new StringBuilder();

        while (n1 >0 && n2 >0){
            if(aArr[n1-1]==bArr[n2-1]){
                builder.append(aArr[n1-1]);
                n1--;
                n2--;
            }else if(dp[n1][n2-1] > dp[n1-1][n2]){
                builder.append(bArr[n2-1]);
                n2--;
            }else{
                builder.append(aArr[n1-1]);
                n1--;
            }
        }

        while(n1>0){
            builder.append(aArr[n1-1]);
            n1--;
        }

        while(n2>0){
            builder.append(bArr[n2-1]);
            n2--;
        }

        return String.valueOf(builder.reverse());
    }
}
