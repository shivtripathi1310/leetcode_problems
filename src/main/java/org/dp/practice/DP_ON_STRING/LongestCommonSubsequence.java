package org.dp.practice.DP_ON_STRING;

import java.util.Arrays;

public class LongestCommonSubsequence {
    public static void main(String[] args) {
        String str01 = "abcba";
        String str02 = "abcbcba";
        int m = str01.length();
        int n = str02.length();

        int[][] dp = new int[m][n];
        for (int i = 0; i < m; i++)
            Arrays.fill(dp[i], -1);

        System.out.println(lcsRecursion(str01, str02, m - 1, n - 1));
        System.out.println(lcsMemoization(str01, str02, m - 1, n - 1, dp));
        System.out.println(lcsBottomUp01(str01, str02, m - 1, n - 1));
        System.out.println(lcsBottomUp02(str01, str02, m, n));
        System.out.println(lcsBottomUpSpaceOptimisation01(str01, str02, m, n));
    }

    //Recursion TC: O(3^n) , SC: O(m+n) -> stack space
    private static int lcsRecursion(String str01, String str02, int m, int n) {

        //Outbound BaseCase
        if (m < 0 || n < 0)
            return 0;

        int pick = Integer.MIN_VALUE;
        if (str01.charAt(m) == str02.charAt(n))
            pick = 1 + lcsRecursion(str01, str02, m - 1, n - 1);
        int notPick01 = lcsRecursion(str01, str02, m - 1, n);
        int notPick02 = lcsRecursion(str01, str02, m, n - 1);

        return Math.max(pick, Math.max(notPick01, notPick02));
    }

    //Memoization TC: O(m*n) , SC: O(m*n) + O(m+n) -> stack space
    private static int lcsMemoization(String str01, String str02, int m, int n, int[][] dp) {

        //Outbound BaseCase
        if (m < 0 || n < 0)
            return 0;

        if (dp[m][n] != -1)
            return dp[m][n];

        int pick = Integer.MIN_VALUE;
        if (str01.charAt(m) == str02.charAt(n))
            pick = 1 + lcsMemoization(str01, str02, m - 1, n - 1, dp);
        int notPick01 = lcsMemoization(str01, str02, m - 1, n, dp);
        int notPick02 = lcsMemoization(str01, str02, m, n - 1, dp);

        return dp[m][n] = Math.max(pick, Math.max(notPick01, notPick02));
    }

    //BottomUp Approach 01 - Here 0th index of dp is treated as 0th index of String array as usual
    //1st index of dp is treated as 1st index of String arrays.
    //TC: O(m*n) , SC: O(m*n)
    private static int lcsBottomUp01(String str01, String str02, int m, int n) {
        int[][] dp = new int[m + 1][n + 1];

        for (int i = 0; i <= m; i++) {
            for (int j = 0; j <= n; j++) {

                int pick = 0;
                int notPick01 = 0;
                int notPick02 = 0;
                if (str01.charAt(i) == str02.charAt(j)) {
                    pick = 1;
                    if ((i - 1) >= 0 && (j - 1) >= 0)
                        pick += dp[i - 1][j - 1];
                }
                if ((i - 1) >= 0)
                    notPick01 = dp[i - 1][j];
                if ((j - 1) >= 0)
                    notPick02 = dp[i][j - 1];

                dp[i][j] = Math.max(pick, Math.max(notPick01, notPick02));
            }
        }

        return dp[m][n];
    }

    //BottomUp Approach 02 - Here 0th index of dp is treated as -1 or negative
    //1st index of dp is treated as 0th index of String arrays.
    //Negative index Right shift approach to be used only when we have Outbound Base Cases OR cases when 0th row/col are left unfilled.
    //TC: O(m*n) , SC: O(m*n)
    private static int lcsBottomUp02(String str01, String str02, int m, int n) {
        int[][] dp = new int[m + 1][n + 1];

        for (int i = 0; i <= m; i++) {
            for (int j = 0; j <= n; j++) {
                if (i == 0 || j == 0) {
                    dp[i][j] = 0;
                }
            }
        }

        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {

                int pick = 0;
                int notPick01 = 0;
                int notPick02 = 0;
                if (str01.charAt(i - 1) == str02.charAt(j - 1)) {
                    pick = 1 + dp[i - 1][j - 1];
                }

                notPick01 = dp[i - 1][j];
                notPick02 = dp[i][j - 1];

                dp[i][j] = Math.max(pick, Math.max(notPick01, notPick02));
            }
        }

        return dp[m][n];
    }

    //BottomUp + Space Optimisation, Approach 01
    //TC: O(m*n) , SC: O(n)..
    private static int lcsBottomUpSpaceOptimisation01(String str01, String str02, int m, int n) {
        int[] prevRow = new int[n + 1];

        for (int i = 0; i <= m; i++) {
            for (int j = 0; j <= n; j++) {
                if (i == 0 || j == 0) {
                    prevRow[j] = 0;
                }
            }
        }

        for (int i = 1; i <= m; i++) {
            int[] temp = new int[n + 1];
            temp[0] = 0;
            for (int j = 1; j <= n; j++) {

                int pick = 0;
                int notPick01 = 0;
                int notPick02 = 0;
                if (str01.charAt(i - 1) == str02.charAt(j - 1)) {
                    pick = 1 + prevRow[j - 1];
                }

                notPick01 = prevRow[j];
                notPick02 = temp[j - 1];

                temp[j] = Math.max(pick, Math.max(notPick01, notPick02));
            }
            prevRow = temp;
        }

        return prevRow[n];
    }
}
