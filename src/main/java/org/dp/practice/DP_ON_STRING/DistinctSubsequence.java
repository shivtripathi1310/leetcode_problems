package org.dp.practice.DP_ON_STRING;

import java.util.Arrays;

/**
 * Here , we have to find total no. of ways the "t" string is present in "s" string.
 */
public class DistinctSubsequence {
    public static void main(String[] args) {
        String s = "babgbag";
        String t = "bag";
        int m = s.length();
        int n = t.length();
        int[][] dp = new int[m+1][n+1];
        for(int i=0;i<=m;i++){
            Arrays.fill(dp[i],-1);
        }

        System.out.println(totalWaysRecursion(s,t,m-1,n-1));
        System.out.println(totalWaysMemoization(s,t,m-1,n-1,dp));
        System.out.println(totalWaysBottomUp(s,t));
        System.out.println(totalWaysBottomUpSpaceOptimisation(s,t));
    }

    //Recursion TC: O(3^n) , SC: O(m+n) -> stack space
    private static int totalWaysRecursion(String s, String t, int m,int n){
        // BC when target string is exhausted, i.e it got matched fully
        if(n<0)
            return 1;

        // BC when source string is exhausted, but target string is still present i.e it did not got matched fully
        if(m<0)
            return 0;

        if(s.charAt(m)==t.charAt(n)){
            return totalWaysRecursion(s,t,m-1,n-1) + totalWaysRecursion(s,t,m-1,n);
        }else
            return totalWaysRecursion(s,t,m-1,n);
    }

    //Memoization TC: O(m*n) , SC: O(m*n) + O(m+n) -> stack space
    private static int totalWaysMemoization(String s, String t, int m,int n, int[][] dp){
        // BC when target string is exhausted, i.e it got matched fully
        if(n<0)
            return 1;

        // BC when source string is exhausted, but target string is still present i.e it did not got matched fully
        if(m<0)
            return 0;

        if(dp[m][n] != -1) return dp[m][n];

        if(s.charAt(m)==t.charAt(n)){
            return dp[m][n] = totalWaysMemoization(s,t,m-1,n-1,dp) + totalWaysMemoization(s,t,m-1,n,dp);
        }else
            return dp[m][n] = totalWaysMemoization(s,t,m-1,n,dp);
    }

    //BottomUp TC: O(m*n) , SC: O(m*n)
    private static int totalWaysBottomUp(String s, String t){
        int m = s.length();
        int n = t.length();
        int[][] dp =  new int[m+1][n+1];

        for(int i=0;i<=m;i++){
            for(int j=0;j<=n;j++){
                if(i==0)
                    dp[i][j] = 0;
                else if(j==0)
                    dp[i][j] = 1;
            }
        }
        dp[0][0] = 1;
        for(int i=1;i<=m;i++){
            for(int j=1;j<=n;j++){

                if(s.charAt(i-1)==t.charAt(j-1)){
                    dp[i][j] = dp[i-1][j-1] + dp[i-1][j];
                }else
                    dp[i][j] = dp[i-1][j];
            }
        }

        return dp[m][n];
    }

    //BottomUp + Space Optimisation, Approach 01 TC: O(m*n) , SC: O(n)..
    private static int totalWaysBottomUpSpaceOptimisation(String s, String t){
        int m = s.length();
        int n = t.length();
        int[] prevRow =  new int[n+1];

        for(int i=0;i<=m;i++){
            for(int j=0;j<=n;j++){
                if(i==0)
                    prevRow[j] = 0;
                else if(j==0)
                    prevRow[j] = 1;
            }
        }
        prevRow[0] = 1;
        for(int i=1;i<=m;i++){
            int[] temp =  new int[n+1];
            temp[0] = 1;
            for(int j=1;j<=n;j++){

                if(s.charAt(i-1)==t.charAt(j-1)){
                    temp[j] = prevRow[j-1] + prevRow[j];
                }else
                    temp[j] = prevRow[j];
            }
            prevRow = temp;
        }

        return prevRow[n];
    }
}
