package org.dp.practice.DP_ON_STOCKS;

/**
 * price[] is given, to buy and sell stock for each day
 * user can buy and sell only once
 * Find max profit
 */
public class BuyAndSellStock_01 {
    public static void main(String[] args) {
        int[] price = {7,1,3,4,6};

        System.out.println(maxProfit(price));
    }

    // To get max profit, iterate from 1th-(n-1)th index
    // Getting min value from left side of array and profit = price[i] - minValue
    private static int maxProfit(int[] price) {

        //let price[0] be the min value Initial
        int min = price[0];
        int profit = Integer.MIN_VALUE;

        // To get max profit, iterate from 1th-(n-1)th index
        // Getting min value from left side of array and profit = price[i] - minValue
        for (int i=1;i<price.length;i++){
            //Getting max profit
            profit = Math.max(profit,price[i] - min);

            //Getting min value from left side of array
            min = Math.min(min,price[i]);
        }

        return profit;
    }
}
