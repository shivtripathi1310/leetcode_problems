package org.dp.practice.DP_ON_SUBSEQUENCE;

/**
 * https://leetcode.com/problems/coin-change-ii/description/
 */
public class CoinChange_TotalWaysForAmount {
    public static void main(String[] args) {
        int[] coins = {1,2,5};
        int amount = 4;
        int n = coins.length;

        int val = totalWaysRecursion(coins, n - 1, amount);
        System.out.println(val == Integer.MAX_VALUE - 1 ? -1 : val);

        System.out.println(totalWaysBottomUp(coins,n-1,amount));
        System.out.println(totalWaysBottomUpSpaceOptimisation(coins,n-1,amount));
    }

    //Recursion TC: O(2^n) even more , SC: O(n) even more -> stack space
    private static int totalWaysRecursion(int[] coins,int n, int amount){
        if(amount==0)
            return 1;
        if(n==0){
            if(amount % coins[0] == 0)
                return 1;
            else
                return 0;
        }

        int pick = 0;
        if(coins[n]<=amount){
            pick = totalWaysRecursion(coins,n,amount-coins[n]);
        }
        int notPick = totalWaysRecursion(coins,n-1,amount);

       return pick + notPick;
    }

    //BottomUp TC: O(m*n) , SC: O(m*n)
    private static int totalWaysBottomUp(int[] coins,int n, int amount){
        int[][] dp = new int[coins.length][amount+1];


        for(int i=0;i<coins.length;i++){
            for(int j=0;j<=amount;j++){
                if(j==0)
                    dp[i][j] = 1;
                else if(i==0){
                    if(j % coins[i] == 0)
                        dp[i][j] = 1;
                    else
                        dp[i][j] = 0;
                }
            }
        }

        for(int i=1;i<coins.length;i++){
            for(int j=1;j<=amount;j++){

                int pick = 0;
                if(coins[i]<=j){
                    pick = dp[i][j-coins[i]];
                }
                int notPick = dp[i-1][j];

                dp[i][j]= pick + notPick;
            }
        }

        return dp[n][amount];
    }

    //BottomUp + Space Optimisation TC: O(m*n) , SC: O(n)
    private static int totalWaysBottomUpSpaceOptimisation(int[] coins,int n, int amount){
        int[] prevRow = new int[amount+1];


        for(int i=0;i<coins.length;i++){
            for(int j=0;j<=amount;j++){
                if(j==0)
                    prevRow[j] = 1;
                else if(i==0){
                    if(j % coins[i] == 0)
                        prevRow[j] = 1;
                    else
                        prevRow[j] = 0;
                }
            }
        }

        for(int i=1;i<coins.length;i++){
            int[] temp = new int[amount+1];
            temp[0] = 1;
            for(int j=1;j<=amount;j++){

                int pick = 0;
                if(coins[i]<=j){
                    pick = temp[j-coins[i]];
                }
                int notPick = prevRow[j];

                temp[j]= pick + notPick;
            }
            prevRow = temp;
        }

        return prevRow[amount];
    }
}
