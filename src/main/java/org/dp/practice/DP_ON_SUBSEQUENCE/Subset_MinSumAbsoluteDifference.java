package org.dp.practice.DP_ON_SUBSEQUENCE;

public class Subset_MinSumAbsoluteDifference {
    public static void main(String[] args) {
        int[] nums = {5, 3, 2, 4};
        int total = 0;
        for(int val : nums)
            total += val;

        Boolean[][] dp = new Boolean[nums.length][total + 1];
        System.out.println(minSubsetSumDiffBottomUp(nums, nums.length-1,total));
    }

    //Bottom Up
    private static int minSubsetSumDiffBottomUp(int[] nums, int n, int total) {
        Boolean[][] dp = new Boolean[nums.length][total + 1];

        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j <= total; j++) {
                if (j == 0)
                    dp[i][j] = true;
                else if (i == 0) {
                    if (nums[i] == j)
                        dp[i][j] = true;
                    else
                        dp[i][j] = false;
                }
            }
        }

        for (int i = 1; i < nums.length; i++) {
            for (int j = 1; j <= total; j++) {

                boolean pick = false;
                if (nums[i] <= j)
                    pick = dp[i - 1][j - nums[i]];
                boolean notPick = dp[i - 1][j];

                dp[i][j] = pick | notPick;
            }
        }

        // Finding minimum value
        int min = Integer.MAX_VALUE;
        for(int j=0;j<=total/2;j++){
            if(dp[n][j]==true)
                min = Math.min(min,Math.abs(total-2*j));
        }
        return min;
    }
}
