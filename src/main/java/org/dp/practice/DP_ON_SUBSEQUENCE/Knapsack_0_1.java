package org.dp.practice.DP_ON_SUBSEQUENCE;

import java.util.Arrays;

public class Knapsack_0_1 {
    public static void main(String[] args) {
        int[] wt = {2,1,4,5};
        int[] val ={1,4,5,1};
        int totalWt = 9;
        int[][] dp = new int[wt.length][totalWt+1];
        for(int i =0;i<wt.length;i++)
            Arrays.fill(dp[i],-1);

        System.out.println(maxProfit(wt, val, wt.length - 1, totalWt));
        System.out.println(maxProfitMemoization(wt, val, wt.length - 1, totalWt,dp));
        System.out.println(maxProfitBottomUp(wt, val, wt.length - 1, totalWt));
        System.out.println(maxProfitBottomUpSpaceOptimisation01(wt, val, wt.length - 1, totalWt));
        System.out.println(maxProfitBottomUpSpaceOptimisation02(wt, val, wt.length - 1, totalWt));
    }

    //Recursion TC: O(2^n) , SC: O(n) -> stack space
    private static int maxProfit(int[] wt, int[] val, int n, int total) {

        if(total == 0)
            return 0;

        if(n==0){
            if(wt[n]<=total)
                return val[n];
            return 0;
        }

        int pick = Integer.MIN_VALUE;
        if(wt[n]<=total)
            pick = val[n] + maxProfit(wt, val, n-1, total-wt[n]);
        int notPick = maxProfit(wt, val, n-1, total);

        return Math.max(pick,notPick);
    }

    //Memoization TC: O(m*n) , SC: O(m*n) + O(n) -> stack space
    private static int maxProfitMemoization(int[] wt, int[] val, int n, int total,int[][] dp) {

        if(total == 0)
            return 0;

        if(n==0){
            if(wt[n]<=total)
                return val[n];
            return 0;
        }

        if(dp[n][total] !=-1) return dp[n][total];

        int pick = Integer.MIN_VALUE;
        if(wt[n]<=total)
            pick = val[n] + maxProfit(wt, val, n-1, total-wt[n]);
        int notPick = maxProfit(wt, val, n-1, total);

        return dp[n][total] = Math.max(pick,notPick);
    }

    //BottomUp TC: O(m*n) , SC: O(m*n)
    private static int maxProfitBottomUp(int[] wt, int[] val, int n, int total) {
        int[][] dp = new int[wt.length][total+1];

        for(int i =0;i<wt.length;i++){
            for(int j =0;j<=total;j++){
                if(j==0)
                    dp[i][j] = 0;
                else if(i==0){
                    if(wt[i]<=j)
                        dp[i][j] = val[i];
                    else
                        dp[i][j] = 0;
                }
            }
        }

        for(int i =1;i<wt.length;i++){
            for(int j =1;j<=total;j++){

                int pick = Integer.MIN_VALUE;
                if(wt[i]<=j)
                    pick = val[i] + dp[i-1][j-wt[i]];
                int notPick =  dp[i-1][j];

                dp[i][j] = Math.max(pick,notPick);
            }
        }

        return dp[n][total] ;
    }

    //BottomUp +  Space Optimisation 01, TC: O(m*n) , SC: O(n) + O(n)..
    private static int maxProfitBottomUpSpaceOptimisation01(int[] wt, int[] val, int n, int total) {
        int[] prevRow = new int[total+1];

        for(int i =0;i<wt.length;i++){
            for(int j =0;j<=total;j++){
                if(j==0)
                    prevRow[j] = 0;
                else if(i==0){
                    if(wt[i]<=j)
                        prevRow[j] = val[i];
                    else
                        prevRow[j] = 0;
                }
            }
        }

        for(int i =1;i<wt.length;i++){
            int[] temp = new int[total+1];
            temp[0] = 0;
            for(int j =1;j<=total;j++){

                int pick = Integer.MIN_VALUE;
                if(wt[i]<=j)
                    pick = val[i] + prevRow[j-wt[i]];
                int notPick =  prevRow[j];

                temp[j] = Math.max(pick,notPick);
            }
            prevRow = temp;
        }

        return prevRow[total] ;
    }

    //BottomUp +  Space Optimisation 02, TC: O(m*n) , SC: O(n)
    //Here , temp arr is not used , here we are using the same prevRow Array and Appending it.
    //moving from right->left
    private static int maxProfitBottomUpSpaceOptimisation02(int[] wt, int[] val, int n, int total) {
        int[] prevRow = new int[total+1];

        for(int i =0;i<wt.length;i++){
            for(int j =0;j<=total;j++){
                if(j==0)
                    prevRow[j] = 0;
                else if(i==0){
                    if(wt[i]<=j)
                        prevRow[j] = val[i];
                    else
                        prevRow[j] = 0;
                }
            }
        }

        for(int i =1;i<wt.length;i++){
            for(int j =total;j>=1;j--){

                int pick = Integer.MIN_VALUE;
                if(wt[i]<=j)
                    pick = val[i] + prevRow[j-wt[i]];
                int notPick =  prevRow[j];

                prevRow[j] = Math.max(pick,notPick);
            }
        }

        return prevRow[total] ;
    }
}
