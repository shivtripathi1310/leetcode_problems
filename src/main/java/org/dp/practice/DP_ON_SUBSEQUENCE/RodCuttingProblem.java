package org.dp.practice.DP_ON_SUBSEQUENCE;

import java.util.Arrays;

public class RodCuttingProblem {
    public static void main(String[] args) {
        int length = 5;
        int[] price = {2,2,3,2,1};
        int[][] dp = new int[price.length][length + 1];
        for (int i = 0; i < price.length; i++)
            Arrays.fill(dp[i], -1);

        System.out.println(maxProfitRecursion(price,price.length-1,length));
        System.out.println(maxProfitMemoization(price,price.length-1,length,dp));
        System.out.println(maxProfitBottomUp(price,price.length-1,length));
        System.out.println(maxProfitBottomUpSpaceOptimisation01(price,price.length-1,length));
        System.out.println(maxProfitBottomUpSpaceOptimisation02(price,price.length-1,length));
    }

    //Recursion TC: O(2^n) , SC: O(n) -> stack space
    private static int maxProfitRecursion(int[] price, int n, int length) {

        if (length == 0)
            return 0;

        if (n == 0) {
           return (length/1) * price[n];
        }

        int pick = Integer.MIN_VALUE;
        if ((n+1) <= length)
            pick = price[n] + maxProfitRecursion(price, n, length - n);
        int notPick = maxProfitRecursion(price, n-1, length);

        return Math.max(pick, notPick);
    }

    //Memoization TC: O(m*n) , SC: O(m*n) + O(n) -> stack space
    private static int maxProfitMemoization(int[] price, int n, int length,int[][] dp) {

        if (length == 0)
            return 0;

        if (n == 0) {
            return (length/1) * price[n];
        }

        if (dp[n][length] != -1) return dp[n][length];

        int pick = Integer.MIN_VALUE;
        if ((n+1) <= length)
            pick = price[n] + maxProfitMemoization(price, n, length - n,dp);
        int notPick = maxProfitMemoization(price, n-1, length,dp);

        return dp[n][length] = Math.max(pick, notPick);
    }

    //BottomUp TC: O(m*n) , SC: O(m*n)
    private static int maxProfitBottomUp(int[] price, int n, int length){
        int[][] dp = new int[price.length][length + 1];

        for (int i = 0; i < price.length; i++) {
            for (int j = 0; j <= length; j++) {
                if (j == 0)
                    dp[i][j] = 0;
                else if (i == 0) {
                    dp[i][j] = (j / 1) * price[i];
                }
            }
        }

        for (int i = 1; i < price.length; i++) {
            for (int j = 1; j <= length; j++) {

                int pick = Integer.MIN_VALUE;
                if (i <= j)
                    pick = price[i] + dp[i][j - i];
                int notPick = dp[i - 1][j];

                dp[i][j] = Math.max(pick, notPick);
            }
        }

        return dp[n][length];
    }

    //BottomUp +  Space Optimisation 01, TC: O(m*n) , SC: O(n) + O(n)..
    private static int maxProfitBottomUpSpaceOptimisation01(int[] price, int n, int length){
        int[] prevRow = new int[length + 1];

        for (int i = 0; i < price.length; i++) {
            for (int j = 0; j <= length; j++) {
                if (j == 0)
                    prevRow[j] = 0;
                else if (i == 0) {
                    prevRow[j] = (j / 1) * price[i];
                }
            }
        }

        for (int i = 1; i < price.length; i++) {
            int[] temp = new int[length + 1];
            temp[0] = 0;
            for (int j = 1; j <= length; j++) {

                int pick = Integer.MIN_VALUE;
                if (i <= j)
                    pick = price[i] + temp[j - i];
                int notPick = prevRow[j];

                temp[j] = Math.max(pick, notPick);
            }
            prevRow = temp;
        }

        return prevRow[length];
    }

    //BottomUp +  Space Optimisation 02, TC: O(m*n) , SC: O(n)
    //Here , temp arr is not used , here we are using the same prevRow Array and Appending it.
    //moving from left->right and appending the row value.
    private static int maxProfitBottomUpSpaceOptimisation02(int[] price, int n, int length){
        int[] prevRow = new int[length + 1];

        for (int i = 0; i < price.length; i++) {
            for (int j = 0; j <= length; j++) {
                if (j == 0)
                    prevRow[j] = 0;
                else if (i == 0) {
                    prevRow[j] = (j / 1) * price[i];
                }
            }
        }

        for (int i = 1; i < price.length; i++) {
            for (int j = 1; j <= length; j++) {

                int pick = Integer.MIN_VALUE;
                if (i <= j)
                    pick = price[i] + prevRow[j - i];
                int notPick = prevRow[j];

                prevRow[j] = Math.max(pick, notPick);
            }
        }

        return prevRow[length];
    }
}
