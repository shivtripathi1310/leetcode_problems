package org.dp.practice.DP_ON_1D;

public class HouseRobber01 {
    public static void main(String[] args) {
        int[] arr = {1, 2, 4, 9};
        int[] dp = new int[arr.length];
        System.out.println(maxSum(arr, arr.length - 1));
        System.out.println(maxSum(arr, arr.length - 1, dp));
        System.out.println(maxSumWithSpaceOptimise(arr, arr.length - 1));
    }

    //Recursive Solution
    private static int maxSum(int[] arr, int n) {
        if (n == 0) {
            return arr[0];
        }
        int val01 = arr[n];
        if (n > 1)
            val01 += maxSum(arr, n - 2);
        int val02 = maxSum(arr, n - 1);

        return Math.max(val01, val02);
    }

    //Tabulation : Bottom-Up
    private static int maxSum(int[] arr, int n, int[] dp) {
        dp[0] = arr[0];

        for (int i = 1; i <= n; i++) {
            int val01 = arr[i];
            if (i > 1)
                val01 += dp[i - 2];
            int val02 = dp[i - 1];
            dp[i] = Math.max(val01, val02);
        }
        return dp[n];
    }

    //Tabulation : Bottom-Up with Space optimisation O(1)
    private static int maxSumWithSpaceOptimise(int[] arr, int n) {
        int prev2 = arr[0];
        int prev = 0;
        int curr = 0;

        for (int i = 1; i <= n; i++) {
            int val01 = arr[i];
            if (i > 1)
                val01 += prev2;
            int val02 = prev;
            curr = Math.max(val01, val02);

            //set the value
            prev2 = prev;
            prev = curr;
        }
        return curr;
    }
}
