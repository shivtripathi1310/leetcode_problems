package org.dp.practice.DP_ON_1D;

import java.util.Arrays;

public class HouseRobber02 {
    public static void main(String[] args) {
        int[] arr = {1, 2, 4, 9};
        int n =arr.length;

        int[] dp = new int[n];
        int[] arr01 = Arrays.copyOfRange(arr,1,n);
        int[] arr02 = Arrays.copyOfRange(arr,0,n-1);

        int ans = Math.max(maxSum(arr01,n-2,dp),maxSum(arr02,n-2,dp));
        System.out.println(ans);
    }

    //Tabulation : Bottom-Up
    private static int maxSum(int[] arr, int n, int[] dp) {
        dp[0] = arr[0];

        for (int i = 1; i <= n; i++) {
            int val01 = arr[i];
            if (i > 1)
                val01 += dp[i - 2];
            int val02 = dp[i - 1];
            dp[i] = Math.max(val01, val02);
        }
        return dp[n];
    }
}
