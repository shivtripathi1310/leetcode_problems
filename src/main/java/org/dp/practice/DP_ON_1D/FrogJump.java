package org.dp.practice.DP_ON_1D;

import java.util.Arrays;

/**
 *
 */
public class FrogJump {
    public static void main(String[] args) {
        int[] egy = {10, 20, 30, 10};
        int totalSteps = 4;
        int[] dp = new int[totalSteps];

        //Recursive for 2 steps
        System.out.println(minEnergyLost(egy, totalSteps - 1));
        //Bottom Up for 2 steps
        System.out.println(minEnergyLost(egy, totalSteps - 1, dp));

        //Recursive for K steps
        System.out.println(minEnergyLost(egy, totalSteps - 1,2));
        //Bottom Up for k steps
        System.out.println(minEnergyLost(egy, totalSteps - 1, dp, 2));

    }

    //Recursive for 2 steps
    private static int minEnergyLost(int[] arr, int n) {
        if (n == 0) {
            return 0;
        }
        int min;
        int left = minEnergyLost(arr, n - 1) + Math.abs(arr[n] - arr[n - 1]);
        if (n > 1) {
            int right = minEnergyLost(arr, n - 2) + Math.abs(arr[n] - arr[n - 2]);
            min = Math.min(left, right);
        } else {
            min = left;
        }
        return min;
    }

    //Bottom Up for 2 steps
    private static int minEnergyLost(int[] arr, int n, int[] dp) {
        dp[0] = 0;
        dp[1] = Math.abs(arr[1] - arr[0]);

        for (int i = 2; i <= n; i++) {
            int left = dp[i - 1] + Math.abs(arr[i] - arr[i - 1]);
            int right = dp[i - 2] + Math.abs(arr[i] - arr[i - 2]);
            dp[i] = Math.min(left, right);
        }
        System.out.println(Arrays.toString(dp));
        return dp[n];
    }

    //Bottom Up for k steps
    private static int minEnergyLost(int[] arr, int n, int[] dp, int kSteps) {
        dp[0] = 0;
        dp[1] = Math.abs(arr[1] - arr[0]);

        int min = Integer.MAX_VALUE;
        int value = 0;
        for (int i = 2; i <= n; i++) {
            for (int j = 1; j <= kSteps; j++) {
                if (i - j >= 0) {
                    value = dp[i - j] + Math.abs(arr[i] - arr[i - j]);
                } else
                    break;
                min = Math.min(min, value);
            }

            dp[i] = min;
        }

        return dp[n];
    }

    //Recursive for K steps
    private static int minEnergyLost(int[] arr, int n, int kSteps) {
        if (n == 0) {
            return 0;
        }
        int min = Integer.MAX_VALUE;


        for (int i = 1; i <= kSteps; i++) {
            if(n-i>=0) {
                min = Math.min(min, minEnergyLost(arr, n - i,kSteps) + Math.abs(arr[n] - arr[n - i]));
            }
        }
        return min;
    }
}
