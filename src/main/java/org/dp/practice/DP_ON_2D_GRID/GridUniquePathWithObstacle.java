package org.dp.practice.DP_ON_2D_GRID;

import java.util.Arrays;

public class GridUniquePathWithObstacle {
    public static void main(String[] args) {

        int[][] obsGrid = {{0, 0, 0},
                {0, 1, 0},
                {0, 0, 0}};
        int row = obsGrid.length;
        int col = obsGrid[0].length;

        int[][] dp = new int[row][col];
        int[][] newDp = new int[row][col];
        for (int i = 0; i < row; i++) {
            Arrays.fill(dp[i], -1);
        }

        System.out.println(totalWaysRecursion(row - 1, col - 1, obsGrid));
        System.out.println(totalWaysMemoization(row - 1, col - 1, dp, obsGrid));
        System.out.println(totalWaysBottomUp02(row - 1, col - 1, newDp, obsGrid));
        System.out.println(totalWaysBottomUpWithSpaceOptimisation(row - 1, col - 1, obsGrid));
    }

    //Recursion
    private static int totalWaysRecursion(int row, int col, int[][] grid) {

        // If there is obstacle, return straight 0.
        if (row >= 0 && col >= 0 && grid[row][col] == 1)
            return 0;

        // +1 as it reached destination
        if (row == 0 && col == 0)
            return 1;

        // 0 as it is moving out of grid board
        if (row < 0 || col < 0)
            return 0;

        int top = totalWaysRecursion(row - 1, col, grid);
        int left = totalWaysRecursion(row, col - 1, grid);

        return top + left;
    }

    //Memoization
    private static int totalWaysMemoization(int row, int col, int[][] dp, int[][] grid) {

        // If there is obstacle, return straight 0.
        if (row >= 0 && col >= 0 && grid[row][col] == 1)
            return 0;

        if (row == 0 && col == 0)
            return 1;

        // 0 as it is moving out of grid board
        if (row < 0 || col < 0)
            return 0;

        if (dp[row][col] != -1)
            return dp[row][col];

        int top = totalWaysMemoization(row - 1, col, dp, grid);
        int left = totalWaysMemoization(row, col - 1, dp, grid);

        return dp[row][col] = top + left;
    }

    //Tabulation : Bottom-Up APPROACH 02
    private static int totalWaysBottomUp02(int row, int col, int[][] dp, int[][] grid) {

        for (int i = 0; i <= row; i++) {
            for (int j = 0; j <= col; j++) {

                // If there is obstacle
                if (grid[i][j] == 1)
                    dp[i][j] = 0;
                else if (i == 0 && j == 0)
                    dp[i][j] = 1;
                else {
                    int top = 0, left = 0;
                    if (i > 0)
                        top = dp[i - 1][j];
                    if (j > 0)
                        left = dp[i][j - 1];
                    dp[i][j] = top + left;
                }
            }
        }

        return dp[row][col];
    }

    //Tabulation : Bottom-Up + Space Optimisation
    private static int totalWaysBottomUpWithSpaceOptimisation(int row, int col, int[][] grid) {

        int[] prevRow = new int[col + 1];

        for (int i = 0; i <= row; i++) {

            int[] tempRow = new int[col + 1];
            for (int j = 0; j <= col; j++) {
                if (grid[i][j] == 1)
                    tempRow[j] = 0;
                else if (i == 0 && j == 0)
                    tempRow[j] = 1;
                else {
                    int top = 0, left = 0;
                    if (i > 0)
                        top = prevRow[j];
                    if (j > 0)
                        left = tempRow[j - 1];
                    tempRow[j] = top + left;
                }
            }

            prevRow = tempRow;
        }

        return prevRow[col];
    }

}
