package org.dp.practice.DP_ON_2D_GRID;

import java.util.Arrays;

public class GridUniquePath {
    public static void main(String[] args) {
        int row = 4;
        int col = 2;

        int[][] dp = new int[row][col];
        int[][] newDp = new int[row][col];
        for (int i = 0; i < row; i++) {
            Arrays.fill(dp[i], -1);
        }
        System.out.println(totalWays(row - 1, col - 1));
        System.out.println(totalWays(row - 1, col - 1, dp));
        System.out.println(totalWaysBottomUp01(row - 1, col - 1, newDp));
        System.out.println(totalWaysBottomUp02(row - 1, col - 1, newDp));
        System.out.println(totalWaysBottomUpWithSpaceOptimisation01(row - 1, col - 1));
        System.out.println(totalWaysBottomUpWithSpaceOptimisation02(row - 1, col - 1));
    }

    //Recursion
    private static int totalWays(int row, int col) {
        // +1 as it reached destination
        if (row == 0 && col == 0)
            return 1;

        // 0 as it is moving out of grid board
        if (row < 0 || col < 0)
            return 0;

        int top = totalWays(row - 1, col);
        int left = totalWays(row, col - 1);

        return top + left;
    }

    //Memoization
    private static int totalWays(int row, int col, int[][] dp) {

        if (row == 0 && col == 0)
            return 1;

        // 0 as it is moving out of grid board
        if (row < 0 || col < 0)
            return 0;

        if (dp[row][col] != -1)
            return dp[row][col];

        int top = totalWays(row - 1, col);
        int left = totalWays(row, col - 1);

        return dp[row][col] = top + left;
    }

    //Tabulation : Bottom-Up APPROACH 01
    private static int totalWaysBottomUp01(int row, int col, int[][] dp) {

        for (int i = 0; i <= row; i++) {
            for (int j = 0; j <= col; j++) {

                if (i == 0 && j == 0)
                    dp[i][j] = 0;
                else if (i == 0 || j == 0)
                    dp[i][j] = 1;
            }
        }

        for (int i = 1; i <= row; i++) {
            for (int j = 1; j <= col; j++) {
                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
            }
        }
        return dp[row][col];
    }

    //Tabulation : Bottom-Up APPROACH 02
    private static int totalWaysBottomUp02(int row, int col, int[][] dp) {

        for (int i = 0; i <= row; i++) {
            for (int j = 0; j <= col; j++) {

                if (i == 0 && j == 0)
                    dp[i][j] = 1;
                else {
                    int top = 0, left = 0;
                    if (i > 0)
                        top = dp[i - 1][j];
                    if (j > 0)
                        left = dp[i][j - 1];
                    dp[i][j] = top + left;
                }
            }
        }

        return dp[row][col];
    }

    //Tabulation : Bottom-Up + Space Optimisation
    private static int totalWaysBottomUpWithSpaceOptimisation01(int row, int col) {

        int[] prevRow = new int[col + 1];
        int[] prevCol = new int[row + 1];

        for (int i = 0; i <= row; i++) {
            for (int j = 0; j <= col; j++) {

                if (i == 0 && j == 0) {
                    prevRow[i] = 0;
                    prevCol[j] = 0;
                } else if (i == 0) {
                    prevRow[j] = 1;
                } else if (j == 0) {
                    prevCol[i] = 1;
                }
            }
        }

        for (int i = 1; i <= row; i++) {
            int[] tempRow = new int[col + 1];
            for (int j = 1; j <= col; j++) {
                tempRow[j] = prevRow[j];
                if (j == 1)
                    tempRow[j] += prevCol[i];
                else
                    tempRow[j] += tempRow[j - 1];
            }
            prevRow = tempRow;
        }

        return prevRow[col];
    }

    //Tabulation : Bottom-Up + Space Optimisation : Approach 02
    private static int totalWaysBottomUpWithSpaceOptimisation02(int row, int col) {

        int[] prevRow = new int[col + 1];
        Arrays.fill(prevRow,0);  // Assigning prev Row with 0

        for (int i = 0; i <= row; i++) {
            int[] tempRow = new int[col + 1];
            for (int j = 0; j <= col; j++) {

                if (i == 0 && j == 0) {
                    tempRow[j] = 1;
                } else {
                    int top = 0;
                    int left =0;
                    if(i>0) {
                        top = prevRow[j];
                    }
                    if(j>0) {
                        left = tempRow[j - 1];
                    }

                    tempRow[j] = top + left;
                }
            }
            prevRow = tempRow;
        }

        return prevRow[col];
    }
}
