package org.dp.practice.DP_ON_2D_GRID;

import java.util.Arrays;

public class MinimumPathSum {
    public static void main(String[] args) {
        int[][] grid = {{5, 10, 0},
                {65, 1, 40},
                {6, 34, 3}};

        int row = grid.length;
        int col = grid[0].length;

        int[][] dp = new int[row][col];
        int[][] newDp = new int[row][col];
        for (int i = 0; i < row; i++)
            Arrays.fill(dp[i], -1);

        System.out.println(minPathRecursion(row - 1, col - 1, grid));
        System.out.println(minPathMemoization(row - 1, col - 1, grid, dp));
        System.out.println(minPathBottomUp(row - 1, col - 1, grid, newDp));
        System.out.println(minPathBottomUpSpaceOptimisation(row - 1, col - 1, grid));
    }

    //Recursion
    private static int minPathRecursion(int row, int col, int[][] grid) {

        if (row == 0 && col == 0)
            return grid[row][col];

        int top = Integer.MAX_VALUE;
        int left = Integer.MAX_VALUE;
        if (row > 0)
            top = grid[row][col] + minPathRecursion(row - 1, col, grid);
        if (col > 0)
            left = grid[row][col] + minPathRecursion(row, col - 1, grid);

        return Math.min(top, left);
    }

    //Memoization
    private static int minPathMemoization(int row, int col, int[][] grid, int[][] dp) {

        if (row == 0 && col == 0)
            return grid[row][col];

        if (dp[row][col] != -1)
            return dp[row][col];

        int top = Integer.MAX_VALUE;
        int left = Integer.MAX_VALUE;
        if (row > 0)
            top = grid[row][col] + minPathMemoization(row - 1, col, grid, dp);
        if (col > 0)
            left = grid[row][col] + minPathMemoization(row, col - 1, grid, dp);

        return dp[row][col] = Math.min(top, left);
    }

    //Tabulation : Bottom Up
    private static int minPathBottomUp(int row, int col, int[][] grid, int[][] dp) {

        for (int i = 0; i <= row; i++) {
            for (int j = 0; j <= col; j++) {

                if (i == 0 && j == 0)
                    dp[i][j] = grid[i][j];
                else {
                    int top = Integer.MAX_VALUE;
                    int left = Integer.MAX_VALUE;
                    if (i > 0)
                        top = grid[i][j] + dp[i - 1][j];
                    if (j > 0)
                        left = grid[i][j] + dp[i][j - 1];

                    dp[i][j] = Math.min(top, left);
                }
            }
        }

        for (int i = 0; i <= row; i++)
            System.out.println(Arrays.toString(dp[i]));

        return dp[row][col];
    }

    //Tabulation : Bottom Up + Space Optimisation
    private static int minPathBottomUpSpaceOptimisation(int row, int col, int[][] grid) {

        int[] prevRow = new int[col+1];

        for (int i = 0; i <= row; i++) {
            int[] tempRow = new int[col+1];
            for (int j = 0; j <= col; j++) {

                if (i == 0 && j == 0)
                    tempRow[j] = grid[i][j];
                else {
                    int top = Integer.MAX_VALUE;
                    int left = Integer.MAX_VALUE;
                    if (i > 0)
                        top = grid[i][j] + prevRow[j];
                    if (j > 0)
                        left = grid[i][j] + tempRow[j - 1];

                    tempRow[j] = Math.min(top, left);
                }
            }
            prevRow = tempRow;
        }
        System.out.println(Arrays.toString(prevRow));
        return prevRow[col];
    }
}
