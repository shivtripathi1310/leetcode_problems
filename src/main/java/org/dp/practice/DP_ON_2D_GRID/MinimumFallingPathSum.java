package org.dp.practice.DP_ON_2D_GRID;

import java.util.Arrays;

public class MinimumFallingPathSum {
    public static void main(String[] args) {

        int[][] grid = {{5, 10, 0},
                {65, 1, 40},
                {6, 34, 3}};

        int row = grid.length;
        int col = grid[0].length;

        int min = Integer.MAX_VALUE;
        for (int i = 0; i < col; i++) {
            min = Integer.min(min, minPathSumRecursion(row - 1, i, grid));
        }
        System.out.println(min);

        min = Integer.MAX_VALUE;
        for (int i = 0; i < col; i++) {
            int[][] dp = new int[row][col];
            for (int j = 0; j < row; j++)
                Arrays.fill(dp[j], -1);

            min = Integer.min(min, minPathSumMemoization(row - 1, i, grid, dp));
        }
        System.out.println(min);

        System.out.println(minPathSumBottomUp(row - 1, col -1 , grid));
        System.out.println(minPathSumBottomUpSpaceOptimisation(row - 1, col -1 , grid));
    }

    //Recursion
    private static int minPathSumRecursion(int row, int col, int[][] grid) {

        if (row == 0)
            return grid[row][col];

        int leftDiag = Integer.MAX_VALUE;
        int rightDiag = Integer.MAX_VALUE;

        if (col > 0)
            leftDiag = grid[row][col] + minPathSumRecursion(row - 1, col - 1, grid);

        int up = grid[row][col] + minPathSumRecursion(row - 1, col, grid);

        if (col < grid[0].length - 1)
            rightDiag = grid[row][col] + minPathSumRecursion(row - 1, col + 1, grid);

        return Math.min(leftDiag, Math.min(up, rightDiag));
    }

    //Memoization
    private static int minPathSumMemoization(int row, int col, int[][] grid, int[][] dp) {

        if (row == 0)
            return grid[row][col];

        if (dp[row][col] != -1)
            return dp[row][col];

        int leftDiag = Integer.MAX_VALUE;
        int rightDiag = Integer.MAX_VALUE;

        if (col > 0)
            leftDiag = grid[row][col] + minPathSumMemoization(row - 1, col - 1, grid, dp);

        int up = grid[row][col] + minPathSumMemoization(row - 1, col, grid, dp);

        if (col < grid[0].length - 1)
            rightDiag = grid[row][col] + minPathSumMemoization(row - 1, col + 1, grid, dp);

        return dp[row][col] = Math.min(leftDiag, Math.min(up, rightDiag));
    }

    //Tabulation - BottomUp
    private static int minPathSumBottomUp(int row, int col, int[][] grid) {
        int[][] dp = new int[row+1][col+1];

        for (int i = 0; i <= row; i++) {
            for (int j = 0; j <= col; j++) {

                if (i == 0)
                    dp[i][j] = grid[i][j];
                else {
                    int leftDiag = Integer.MAX_VALUE;
                    int rightDiag = Integer.MAX_VALUE;

                    int up = grid[i][j] + dp[i - 1][j];
                    if (j > 0)
                        leftDiag = grid[i][j] + dp[i - 1][j - 1];
                    if (j < grid[0].length - 1)
                        rightDiag = grid[i][j] + dp[i - 1][j + 1];

                    dp[i][j] = Math.min(leftDiag, Math.min(rightDiag, up));
                }
            }
        }
        for (int i = 0; i <= row; i++)
            System.out.println(Arrays.toString(dp[i]));

        int min = Integer.MAX_VALUE;
        for (int i = 0; i <= col; i++) {
            min = Math.min(min,dp[row][i]);
        }
        return min;
    }


    //Tabulation - BottomUp + Space Optimisation
    private static int minPathSumBottomUpSpaceOptimisation(int row, int col, int[][] grid) {
        int[] prevRow = new int[col+1];

        for (int i = 0; i <= row; i++) {
            int[] temp = new int[col+1];
            for (int j = 0; j <= col; j++) {

                if (i == 0)
                    temp[j] = grid[i][j];
                else {
                    int leftDiag = Integer.MAX_VALUE;
                    int rightDiag = Integer.MAX_VALUE;

                    int up = grid[i][j] + prevRow[j];
                    if (j > 0)
                        leftDiag = grid[i][j] + prevRow[j - 1];
                    if (j < grid[0].length - 1)
                        rightDiag = grid[i][j] + prevRow[j + 1];

                    temp[j] = Math.min(leftDiag, Math.min(rightDiag, up));
                }
            }
            prevRow = temp;
        }
        System.out.println(Arrays.toString(prevRow));

        int min = Integer.MAX_VALUE;
        for (int i = 0; i <= col; i++) {
            min = Math.min(min,prevRow[i]);
        }
        return min;
    }
}
