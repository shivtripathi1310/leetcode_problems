package org.dp.lcs;

/**
 * Two string are given , we have to find Longest common subsequence.
 * x- abcd
 * y- bd
 * LCS -> bd - 2
 */
public class LCSubsequenceBottomUp {
    public static void main(String[] args) {
        String x = "abcde";
        String y = "bce";

        int[][] dp = new int[x.length() + 1][y.length() + 1];

        System.out.println(findLCS(dp, x, y));
        System.out.println("LCSubsequence is : " + backtrackLCSubsequence(dp,x,y).reverse());
    }

    private static int findLCS(int[][] dp, String x, String y) {

        int xLength = x.length();
        int yLength = y.length();
        char[] xArr = x.toCharArray();
        char[] yArr = y.toCharArray();

        for (int i = 0; i <= xLength; i++) {
            for (int j = 0; j <= yLength; j++) {
                if (i == 0 || j == 0) {
                    dp[i][j] = 0;
                }
            }
        }

        for (int i = 1; i <= xLength; i++) {
            for (int j = 1; j <= yLength; j++) {
                if(xArr[i-1]==yArr[j-1]){
                    dp[i][j]= 1 + dp[i-1][j-1];
                }else{
                    dp[i][j]= Math.max(dp[i-1][j],dp[i][j-1]);
                }
            }
        }

        return dp[xLength][yLength];
    }

    private static StringBuilder backtrackLCSubsequence(int[][] dp, String x, String y){
        StringBuilder stringBuilder = new StringBuilder();

        int xLength = x.length();
        int yLength = y.length();

        char[] xArr = x.toCharArray();
        char[] yArr = y.toCharArray();

        while (xLength>0 && yLength >0){
            if(xArr[xLength-1] == yArr[yLength-1]){
                stringBuilder.append(xArr[xLength-1]);
                xLength--;
                yLength--;
            }else if(dp[xLength-1][yLength] > dp[xLength][yLength-1]){
                xLength--;
            }else{
                yLength--;
            }
        }

        return stringBuilder;
    }
}
