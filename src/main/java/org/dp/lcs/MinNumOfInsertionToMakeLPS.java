package org.dp.lcs;

/**
 * A string is given , we have to find min number of Insertion required to make palindromic subsequence.
 * x-> agbcba
 * LCS -> bd - 2
 * LPS = LCS (a, reverse(a))
 * Insertion = x.length() - lps;
 */
public class MinNumOfInsertionToMakeLPS {
    public static void main(String[] args) {
        String a = "agbcba";
        StringBuilder builder = new StringBuilder(a);
        String b= String.valueOf(builder.reverse());

        int[][] dp = new int[a.length() + 1][b.length() + 1];
        int lps = findLCS(dp, a, b);
        System.out.println("LongestPalindromicSubsequence : " +lps);
        System.out.println("MinNumOfInsertionToMakeLPS : " + (a.length() - lps));
    }

    private static int findLCS(int[][] dp, String x, String y) {

        int xLength = x.length();
        int yLength = y.length();
        char[] xArr = x.toCharArray();
        char[] yArr = y.toCharArray();

        for (int i = 0; i <= xLength; i++) {
            for (int j = 0; j <= yLength; j++) {
                if (i == 0 || j == 0) {
                    dp[i][j] = 0;
                }
            }
        }

        for (int i = 1; i <= xLength; i++) {
            for (int j = 1; j <= yLength; j++) {
                if(xArr[i-1]==yArr[j-1]){
                    dp[i][j]= 1 + dp[i-1][j-1];
                }else{
                    dp[i][j]= Math.max(dp[i-1][j],dp[i][j-1]);
                }
            }
        }

        return dp[xLength][yLength];
    }
}
