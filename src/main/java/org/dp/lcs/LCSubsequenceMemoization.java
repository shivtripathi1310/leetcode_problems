package org.dp.lcs;

import java.util.Arrays;

/**
 * Two string are given , we have to find Longest common subsequence.
 * x- abcd
 * y- bd
 * LCS -> bd - 2
 */
public class LCSubsequenceMemoization {
    public static void main(String[] args) {
        String x = "abcde";
        String y = "bce";

        int[][] dp = new int[x.length() + 1][y.length() + 1];

        for(int[] arr : dp){
            Arrays.fill(arr,-1);
        }

        System.out.println(findLCS(x.toCharArray(), y.toCharArray(), x.length(), y.length(),dp));
    }

    private static int findLCS(char[] x, char[] y, int xLength, int yLength, int[][] dp) {

        if(xLength==0 || yLength ==0){
            return 0;
        }

        if(dp[xLength][yLength]!=-1){
            return dp[xLength][yLength];
        }

        if(x[xLength-1] == y[yLength-1]){
            return dp[xLength][yLength] = 1 + findLCS(x, y, xLength-1, yLength-1,dp);
        }else{
            return dp[xLength][yLength] = Math.max(findLCS(x, y, xLength, yLength-1,dp),findLCS(x, y, xLength-1, yLength,dp));
        }
    }


}
