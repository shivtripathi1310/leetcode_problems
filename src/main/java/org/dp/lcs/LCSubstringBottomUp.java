package org.dp.lcs;

/**
 * Two string are given , we have to find Longest common substring.
 * x- abcd
 * y- bd
 * LCS -> b - 1
 *
 * Only difference betn subsequence and substring is.. subsequence should have relative order while substring should be continous
 */
public class LCSubstringBottomUp {
    public static void main(String[] args) {
        String x = "abcdeabcd";
        String y = "qweaebce";

        int[][] dp = new int[x.length() + 1][y.length() + 1];

        System.out.println(findLCS(dp, x, y));
        System.out.println(printSubstring(dp,x,y).reverse());

    }

    private static int findLCS(int[][] dp, String x, String y) {

        int xLength = x.length();
        int yLength = y.length();
        char[] xArr = x.toCharArray();
        char[] yArr = y.toCharArray();

        for (int i = 0; i <= xLength; i++) {
            for (int j = 0; j <= yLength; j++) {
                if (i == 0 || j == 0) {
                    dp[i][j] = 0;
                }
            }
        }

        for (int i = 1; i <= xLength; i++) {
            for (int j = 1; j <= yLength; j++) {
                if(xArr[i-1]==yArr[j-1]){
                    dp[i][j]= 1 + dp[i-1][j-1];
                }else{
                    // variation from LCSubsequence
                    dp[i][j]= 0;
                }
            }
        }

        // Finding the max value in 2D Array
        int lcs = 0;
        for(int i=0;i<xLength+1;i++){
            for(int j=0;j<yLength+1;j++){
                lcs = Math.max(lcs,dp[i][j]);
            }
        }
        return lcs;

    }

    private static StringBuilder printSubstring(int[][] dp, String x, String y){
        StringBuilder stringBuilder = new StringBuilder();

        int xLength = x.length();
        int yLength = y.length();

        char[] xArr =x.toCharArray();
        char[] yArr =y.toCharArray();
        int lcs = 0;
        int a = 0,b = 0;
        for(int i=0;i<=xLength;i++){
            for(int j=0;j<yLength;j++){
                if(dp[i][j] >= lcs){
                    a= i;
                    b= j;
                    lcs = dp[i][j];
                }
            }
        }
        System.out.println("Max number : " + lcs);
        System.out.println("a,b : " + a + ", " + b);

        while (a>0 && b>0){
            if(xArr[a-1]==yArr[b-1]){
                stringBuilder.append(xArr[a-1]);
                a--;
                b--;
            }else if(dp[a][b]==0){
                break;
            }
        }
        return stringBuilder;
    }
}
