package org.graph;

/**
 * Binary search is always done, on any array or any DS, when it is
 * 1. sorted
 * 2. not duplicate elements
 *
 * Its TC is O(log n)
 */
public class BinarySearch {
    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5,6,7,8};
        int target = -1;

        int low =0;
        int high = arr.length-1;

        System.out.println(isValuePresent(arr,low,high,target));
    }

    private static boolean isValuePresent(int[] arr, int low, int high, int target){

        if(low>high)
            return false;

        int mid = (low + high)/2;
        if(arr[mid]== target)
            return true;
        else if(target > arr[mid]) {
            low = mid + 1;
        } else {
            high = mid - 1;
        }

        return isValuePresent(arr,low, high, target);
    }
}
