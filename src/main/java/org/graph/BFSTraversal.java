package org.graph;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * Bredth First search Traversal
 * Level wise traversal :
 * 1st ly level 1 elements are traversed
 * 2nd level 2 ele and so no, until all element gets traversed
 */
public class BFSTraversal {
    public static void main(String[] args) {

    }

    private static void bfs(ArrayList<ArrayList<Integer>> adjMatrix){

        List<Integer> bfsList = new ArrayList<>();
        Queue<Integer> queue = new ArrayDeque<>();
        boolean[] visited = new boolean[adjMatrix.size()];

        //Let start with 1st node as 0
        queue.add(0);
        visited[0] = true;

        // running the loop , uptil queue is empty
        while (!queue.isEmpty()){

            //fetching the element from queu and adding it to bfslist(ans)
            int node = queue.poll();
            bfsList.add(node);

            //checking on the node added on bfs, for its adjacent element
            //then adding the adjacent ele on queue only if the y are not visited and later updating "true" in visited array
            for(Integer val : adjMatrix.get(node)){
                if(visited[val] == false){

                    queue.add(val);
                    visited[val] = true;
                }
            }
        }
    }
}
