package org.graph;

import java.util.ArrayList;
import java.util.List;

/**
 * Depth First Search
 * Traversing each element to its depth and then picking other element
 */
public class DFSTraversal {
    public static void main(String[] args) {

    }

    private static void dfsOfGraph(ArrayList<ArrayList<Integer>> adjMatrix) {

        List<Integer> dfsList = new ArrayList<>();
        boolean[] visited = new boolean[adjMatrix.size()];

        // Assuming visit of node starts from 0, thus visit it and call dfs at 0
        visited[0] = true;
        dfs(0, visited, dfsList, adjMatrix);

    }

    private static void dfs(int node, boolean[] visited, List<Integer> dfsList,
                            ArrayList<ArrayList<Integer>> adjMatrix) {
        // make node visited and add it to dfs list
        visited[node]=true;
        dfsList.add(node);

        //check for the adjacent ele of node added, and if not visited call dfs on it.
        //here we are calling depth first search for each element
        for(Integer val : adjMatrix.get(node)){
            if(visited[val] == false)
                dfs(val,visited,dfsList,adjMatrix);
        }
    }
}
