package org.graph;

public class FindKthSmallestInTree {
    static class Node {
        int data;
        Node left;
        Node right;

        Node(int data, Node left, Node right) {
            this.data = data;
            this.left = left;
            this.right = right;
        }
    }

    public static void main(String[] args) {
        Node node01 = new Node(1,null,null);
        Node node02 = new Node(2,null,null);
        Node node03 = new Node(3,null,null);
        Node node04 = new Node(4,null,null);
        Node node05 = new Node(5,null,null);

        node03.left = node01;
        node01.right = node02;
        node03.right = node05;
        node05.left = node04;

        System.out.println(getSmallest(3,node03,0));
        inorderTraversal(node03);
        System.out.println();
        preorderTraversal(node03);
        System.out.println();
        postorderTraversal(node03);
    }

    public static int getSmallest(int k, Node root, int count) {
        if (k == count)
            return root.data;
        if (root == null)
            return 0;

        getSmallest(k, root.left, count);
        count++;
        getSmallest(k, root.right, count);

        return root.data;
    }

    private static void inorderTraversal(Node root){
        if(root==null)
            return;

        inorderTraversal(root.left);
        System.out.print("-" + root.data + "-");
        inorderTraversal(root.right);
    }

    private static void preorderTraversal(Node root){
        if(root==null)
            return;

        System.out.print("-" + root.data + "-");
        preorderTraversal(root.left);
        preorderTraversal(root.right);
    }

    private static void postorderTraversal(Node root){
        if(root==null)
            return;

        postorderTraversal(root.left);
        postorderTraversal(root.right);
        System.out.print("-" + root.data + "-");
    }
}
