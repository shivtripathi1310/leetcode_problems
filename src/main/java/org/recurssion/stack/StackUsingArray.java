package org.recurssion.stack;

import java.util.Arrays;

class StackArray{
    int[] arr;
    int size;
    int tail =-1;

    @Override
    public String toString() {
        return "StackArray{" +
                "arr=" + Arrays.toString(arr)+'}';
    }

    StackArray(int size){
        this.size = size;
       arr = new int[size];
    }

    public boolean isFull(){
        return tail ==(size-1);
    }

    public boolean isEmpty(){
        return tail ==-1;
    }
    public void push(int value){
        if(isFull()) {
            System.out.println("Stack is full");
            return;
        }
        tail++;
        arr[tail]=value;
    }

    public int pop(){
        if(isEmpty()) {
            System.out.println("Stack is Empty");
            return -1;
        }
        int top = arr[tail];
        arr[tail] = -1;
        tail--;
        return top;
    }

    public int peek(){
        if(isEmpty()) {
            System.out.println("Stack is Empty");
            return -1;
        }
        int top = arr[tail];
        return top;
    }
}
public class StackUsingArray {
    public static void main(String[] args) {
        StackArray stack = new StackArray(3);
        stack.push(1);
        stack.push(3);
        stack.push(2);

        System.out.println(stack);

        System.out.println(stack.peek());
        System.out.println(stack.pop());
        System.out.println(stack);
    }
}
