package org.recurssion.stack;

import java.util.Stack;

public class DeleteMiddleElementFromStack {

    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.push(3);
        stack.push(2);
        stack.push(1);
        System.out.println(stack);

        int middleEle = (stack.size()/2)+1;
        stack= removeMiddleEle(stack,middleEle);
        System.out.println(stack);
    }

    private static Stack<Integer> removeMiddleEle(Stack<Integer> stack, int middleEle) {
        if(middleEle==1){
            stack.pop();
            return stack;
        }

        int top = stack.pop();
        stack = removeMiddleEle(stack,middleEle-1);
        stack.push(top);
        return stack;
    }
}
