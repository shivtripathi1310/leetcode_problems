package org.recurssion.stack;

import java.util.ArrayDeque;
import java.util.Queue;

class StackQueue{
    @Override
    public String toString() {
        return "StackQueue{" +
                "q1=" + q1 +
                '}';
    }

    Queue<Integer> q1,q2;
    StackQueue(){
        q1 = new ArrayDeque<>();
        q2 = new ArrayDeque<>();
    }

    public void push(int value){
        int top;
        while (!q1.isEmpty()){
            top= q1.poll();
            q2.add(top);
        }
        q1.add(value);

        while (!q2.isEmpty()){
            top= q2.poll();
            q1.add(top);
        }
    }
    public int pop(){
        int top= q1.poll();
        return top;
    }
    public int peek(){
        int top= q1.peek();
        return top;
    }
}

public class StackUsingTwoQueue {
    public static void main(String[] args) {
        StackQueue stack = new StackQueue();
        stack.push(1);
        stack.push(4);
        stack.push(7);
        stack.push(2);

        System.out.println(stack);
        System.out.println(stack.peek());
        System.out.println(stack.pop());
        System.out.println(stack);
    }
}
