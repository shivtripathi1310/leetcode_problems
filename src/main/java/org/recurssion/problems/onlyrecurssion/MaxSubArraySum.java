package org.recurssion.problems.onlyrecurssion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class MaxSubArraySum {
    public static void main(String[] args) {
        int[] arr = {-2,1,3,-4,3};
        System.out.println("New Max sum : " +  maxSum(arr, arr.length-1));
        System.out.println(maxSubArraySum(arr, arr.length));


        ArrayList<Integer> arrList = new ArrayList<>();
        ArrayList<Integer> arrListResult = new ArrayList<>();
        Arrays.stream(arr).forEach(arrList::add);
        maxSubArraySumUsingRecursion(arrListResult,arrList,1);

        System.out.println(arrListResult.stream()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList()).get(0));

        System.out.println(arrListResult.stream()
                .max(Comparator.naturalOrder())
                .get());

        arrListResult.sort(Comparator.reverseOrder());
        System.out.println(arrListResult.get(0));
    }

    private static int maxSubArraySum(int[] arr, int n) {
        int maxSum = arr[0];
        int maxSumEnding = arr[0];

        for(int i=1;i<n;i++){
            maxSumEnding = Math.max(arr[i],maxSumEnding + arr[i]);
            maxSum = Math.max(maxSum,maxSumEnding);
        }
        return maxSum;
    }

    private static void maxSubArraySumUsingRecursion(List<Integer> list, List<Integer> arr, int value){
        if(arr.isEmpty()){
            list.add(value);
            return;
        }

        int sum01 = value + arr.get(arr.size()-1);
        int sum02 = value;
        arr.remove(arr.size()-1);
        maxSubArraySumUsingRecursion(list,arr, sum01);
        maxSubArraySumUsingRecursion(list,arr, sum02);
    }

    private static int maxSum(int[] arr, int n){

        if(n==0)
            return Math.max(arr[n],0);

        int pick = arr[n] + maxSum(arr,n-1);
        int notPick = maxSum(arr,n-1);

        return Math.max(pick,notPick);
    }
}
