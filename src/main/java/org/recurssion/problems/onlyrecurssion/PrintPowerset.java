package org.recurssion.problems.onlyrecurssion;

public class PrintPowerset {
    public static void main(String[] args) {
        String str = "abc";
        String op = "";
        powerset(str, op);
    }

    private static void powerset(String str, String op) {
        if (str.length() == 0) {
            System.out.print("'" + op + "'" + " ");
            return;
        }

        String op01 = op;
        String op02 = op + str.charAt(0);
        str = str.substring(1);

        powerset(str, op01);
        powerset(str, op02);
    }
}
