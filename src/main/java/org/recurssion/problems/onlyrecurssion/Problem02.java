package org.recurssion.problems.onlyrecurssion;

import java.util.HashMap;
import java.util.Map;

public class Problem02 {

    public static void main(String[] args) {
        int[][]graph = {{1,3},{0,2},{1,3},{0,2}};
        System.out.println(isBipartite(graph));
    }

    /**
     * It only passed 60% of test cases. Not the correct solution.
     * @param graph
     * @return
     */
    public static boolean isBipartite(int[][] graph){
        Map<Integer,Integer> colourMap= new HashMap<>();
        int lengthY = graph.length;
        int lengthX;
        if (lengthY%2!=0)
            return false;
        colourMap.put(0,1);
        int flag=2;
        for(int i=0;i<lengthY;i++){
            lengthX = graph[i].length;
            for(int j =0; j<lengthX;j++){
                if(!colourMap.containsKey(graph[i][j])) {
                    colourMap.put(graph[i][j], flag % 2);
                }else{
                    if(colourMap.get(graph[i][j])!=flag%2){
                        return false;
                    }
                }
            }
            flag++;
        }
        return true;
    }

    /**
     * It is correct solution.
     * @param g
     * @return
     */
    public static boolean isBipartiteRecursive(int[][] g) {
        int[] colors = new int[g.length];
        for (int i = 0; i < g.length; i++)
            if (colors[i] == 0 && !validColor(g, colors, 1, i))
                return false;
        return true;

    }

    private static boolean validColor(int[][] g, int[] colors, int color, int node) {
        if (colors[node] != 0)
            return colors[node] == color;
        colors[node] = color;
        for (int adjacent : g[node])
            if (!validColor(g, colors, -color, adjacent))
                return false;
        return true;
    }
}
