package org.recurssion.problems.onlyrecurssion;

public class PermutationWithCaseChange {
    public static void main(String[] args) {
        String str="abc";
        String op="";
        permutationWithCaseChange(str,op);
    }

    private static void permutationWithCaseChange(String str, String op) {
        if(str.length()==0){
            System.out.print("'"+op+"' ");
            return;
        }

        String op01 = op + String.valueOf(str.charAt(0)).toUpperCase();
        String op02 = op + str.charAt(0);

        str = str.substring(1);

        permutationWithCaseChange(str, op01);
        permutationWithCaseChange(str, op02);
    }
}
