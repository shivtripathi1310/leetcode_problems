package org.recurssion.problems.onlyrecurssion;

public class GeneratePassword {
    public static void main(String[] args) {
        int n=3;
        String op="";
        for(int i=1;i<=n;i++) {
            generatePassword(i, op);
        }
    }

    private static void generatePassword(int n,String op) {
        if(n==0){
            System.out.println(op);
            return;
        }
        String op01= op +"x";
        String op02 = op+"y";
        generatePassword(n-1,op01);
        generatePassword(n-1,op02);
    }
}
