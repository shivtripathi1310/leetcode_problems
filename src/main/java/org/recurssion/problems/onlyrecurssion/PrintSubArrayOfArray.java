package org.recurssion.problems.onlyrecurssion;

import java.util.ArrayList;
import java.util.List;

public class PrintSubArrayOfArray {

    public static void main(String[] args) {
        int[] arr ={2,3,4,5};
        List<List<Integer>> list = new ArrayList<>();
        subArray(arr,0,0,list);
        for (List<Integer> integerList : list){
            System.out.println(integerList);
        }
    }

    private static void subArray(int[] arr, int start, int end, List<List<Integer>> list) {

        if(end == arr.length)
            return;

        else if(start > end) {
            subArray(arr, 0, end + 1,list);
        }else{
            List<Integer> subArrayList = new ArrayList<>();
            for(int i=start;i<=end;i++){
                subArrayList.add(arr[i]);
            }
            if(!subArrayList.isEmpty())
                list.add(subArrayList);
            subArray(arr, start + 1 , end,list);
        }
    }
}
