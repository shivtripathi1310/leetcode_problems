package org.recurssion.problems.onlyrecurssion;

import java.util.ArrayList;
import java.util.List;

/**
 * a given string e.g. abc---> abc,acb,bac,bca,cab,cba
 */
public class PrintPermutationOfArray {

    public static void main(String[] args) {

        String str = "abc";

        List<Character> ds = new ArrayList<>(); // For storing the list of ans
        int[] visited = new int[str.length()]; // For storing, the index got visited or not, such that, next time, same index does not get visited
        List<List<Character>> ansList = new ArrayList<>(); // For storing answers of ds list

        printPermutation(str.toCharArray(), ds, visited, ansList);
        System.out.println(ansList);

//        ansList.stream()
//                .flatMap(list -> list.stream()
//                        .map(ele -> String.valueOf(ele))
//                        .reduce((char01, char02) ->  (char01 + char02))
//                        .stream())
//                .forEach(s -> System.out.println(s));

        ansList.stream()
                .flatMap(list -> list.stream()
                        .map(String::valueOf)
                        .reduce(String::concat)
                        .stream())
                .forEach(System.out::println);
    }

    private static void printPermutation(char[] str, List<Character> ds, int[] visited, List<List<Character>> ansList) {

        if (ds.size() == str.length) {
            ansList.add(new ArrayList<>(ds));  // here make sure to add new ArrayList<>(ds), as it will not get added otherwise
            return;     // make sure to return otherwise , it will throw array index out of bound
        }

        //Here, we want to check for all permutation thus, 0> n-1 loop added
        for (int i = 0; i < str.length; i++) {

            if (visited[i] != 1) {
                // before adding index value to ans, it is checked, if it is already added in list, using visited array
                // if not added, add it to list and make visited 1
                ds.add(str[i]);
                visited[i] = 1;

                // later call the recursive fn , to check for other chars
                printPermutation(str, ds, visited, ansList);

                // once recursive call is over, we have to move back to the tree
                // thus , remove ds list top ele and make visited to not visited (i.e 0)
                ds.remove(ds.size() - 1);
                visited[i] = 0;
            }
        }
    }
}
