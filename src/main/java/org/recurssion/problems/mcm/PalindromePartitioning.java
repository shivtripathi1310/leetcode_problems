package org.recurssion.problems.mcm;

/**
 * A string is given find the min no of partition req. such that the substring is also palindrome
 */
public class PalindromePartitioning {
    public static void main(String[] args) {
        String s = "nitik";
        System.out.println("Min number of partition : " + palindromePartition(s, 0, s.length() - 1));
    }

    private static int palindromePartition(String s, int i, int j) {
        if(i>=j)
            return 0;
        if(isPalindrome(s,i,j))
            return 0;
        int min = Integer.MAX_VALUE;
        for(int k=i;k<=j-1;k++){
            int temp = palindromePartition(s,i,k) + palindromePartition(s,k+1,j) + 1;
            min = Math.min(min,temp);
        }
        return min;
    }

    private static boolean isPalindrome(String s, int i, int j) {
        if(i>=j)
            return true;
        if(s.charAt(i)!=s.charAt(j)){
            return false;
        }
        return isPalindrome(s,i+1,j-1);
    }
}
