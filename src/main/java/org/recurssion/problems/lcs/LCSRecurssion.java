package org.recurssion.problems.lcs;

/**
 * Two string are given , we have to find Longest common subsequence.
 * x- abcd
 * y- bd
 * LCS -> bd - 2
 */
public class LCSRecurssion {
    public static void main(String[] args) {
        String x= "abcde";
        String y= "bce";
        System.out.println("\n"+findLCS(x.toCharArray(), y.toCharArray(), x.length(), y.length()));
    }

    private static int findLCS(char[] x, char[] y, int xLength, int yLength) {
        if(xLength==0 || yLength ==0){
            return 0;
        }

        if(x[xLength-1] == y[yLength-1]){
            return 1 + findLCS(x, y, xLength-1, yLength-1);
        }else{
            return Math.max(findLCS(x, y, xLength, yLength-1),findLCS(x, y, xLength-1, yLength));
        }
    }
}
