package org.recurssion.problems.knapsack;

/**
 * Print true/false if in an array its subset product is equal to k.
 * Array values should be +ive
 */
public class SubsetProductRecurssion {
    public static void main(String[] args) {
        int[] ints = {1,4,2,8,3,6};
        double prod= 16;

        System.out.println(subsetProduct(ints, prod, ints.length));
    }

    private static boolean subsetProduct(int[] arr, double prod, int n) {
        if(prod ==1){
            return true;
        }else if(n==0){
            return false;
        }

        if(arr[n-1]<=prod){
            return subsetProduct(arr,prod/arr[n-1],n-1) | subsetProduct(arr,prod,n-1);
        }else{
            return subsetProduct(arr,prod,n-1);
        }
    }
}
