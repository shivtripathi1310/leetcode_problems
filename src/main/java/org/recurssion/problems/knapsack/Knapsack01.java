package org.recurssion.problems.knapsack;

public class Knapsack01 {
    public static void main(String[] args) {
        Integer[] wt = {1, 2, 3, 4, 5};
        Integer[] val = {4, 2, 4, 5, 3};
        int totalWeight = 7;
        System.out.println("Knapsack : Max profit : " + solveKnapsack01(totalWeight, wt, val, wt.length));

//        Map<Integer, List<Integer>> map = new HashMap<>();
//        List<Integer> ele = new ArrayList<>();
//        solveKnapsack02(totalWeight,wt,val,wt.length,map,0,ele);
//        System.out.println(map);
    }

//    private static void solveKnapsack02(int W, Integer[] wt, Integer[] vt, int n, Map<Integer, List<Integer>> map, int profit,List<Integer> ele) {
//
//        if(n==0 || W==0){
//            map.put(profit,ele);
//            return;
//        }
//
//        if(wt[n-1]<=W){
//            int profit01 = profit + vt[n-1];
//            int profit02 = profit;
//            List<Integer> ele01 = new ArrayList<>(ele);
//            ele01.add(wt[n-1]);
//            List<Integer> ele02 = new ArrayList<>(ele);
//            W= W - wt[n-1];
//            solveKnapsack02(W,wt,vt,n-1,map,profit01,ele01);
//            solveKnapsack02(W,wt,vt,n-1,map,profit02,ele02);
//        }else{
//            int profit02 = profit;
//            List<Integer> ele02 = new ArrayList<>(ele);
//            W= W - wt[n-1];
//            solveKnapsack02(W,wt,vt,n-1,map,profit02,ele02);
//        }
//    }

    private static int solveKnapsack01(int totalWeight, Integer[] wt, Integer[] val, int n) {
        if (n == 0 || totalWeight == 0) {
            return 0;
        }
        if (wt[n - 1] <= totalWeight) {
            return Math.max(val[n - 1] + solveKnapsack01(totalWeight - wt[n - 1], wt, val, n - 1), solveKnapsack01(totalWeight, wt, val, n - 1));
        } else {
            return solveKnapsack01(totalWeight, wt, val, n - 1);
        }
    }
}
